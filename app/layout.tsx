"use client";

import { AppContext } from "@/context";
import Header from "./Header";
import "./globals.css";
import { ThemeProvider } from "@mui/material";
import { theme } from "@/styles";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <AppContext>
      <ThemeProvider theme={theme}>
        <html lang="en" style={{ overflowX: "hidden" }}>
          <body
            style={{
              padding: "0 40px",
              position: "relative",
              overflow: "auto",
              height: "100%",
            }}
          >
            <Header />
            {children}
          </body>
        </html>
      </ThemeProvider>
    </AppContext>
  );
}
