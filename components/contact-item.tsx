"use client";

import { Box, Typography } from "@mui/material";
import React, { FC, SyntheticEvent } from "react";

interface ContactItemProps {
  icons: React.JSX.Element;
  title: string;
  href?: Location["href"];
}

const ContactItem: FC<ContactItemProps> = ({ icons, title, href }) => {
  const handleRedirect = (e: SyntheticEvent) => {
    e.preventDefault();
    if (!href) return;
    if (!href.includes("http")) return (window.location.href = href);
    return window.location.replace(href);
  };
  return (
    <Box
      onClick={handleRedirect}
      sx={{
        display: "flex",
        gap: "20px",
        alignItems: "center",
        pl: "20px",
        "&:hover": { opacity: "0.8" },
      }}
    >
      <Box sx={{ cursor: "pointer" }}>{icons}</Box>
      <Typography sx={{ textDecoration: "none", cursor: "pointer" }}>
        {title}
      </Typography>
    </Box>
  );
};

export default ContactItem;
